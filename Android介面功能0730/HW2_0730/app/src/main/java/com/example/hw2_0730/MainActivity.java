package com.example.hw2_0730;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String [] values={"陳振東","馬麗菁","張朝旭"};
    String [] values1={"037-381515","037-381510","037-381520"};
    String [] values2={"教授","教授 兼 管理學院院長","副教授 兼 圖書館系統管理組長"};
    int [] pic = {R.drawable.teacher03,R.drawable.teacher04,R.drawable.teacher05};
    int [] t = {0,0,0};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BaseAdapter baseAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return values.length;
            }

            @Override
            public Object getItem(int i) {
                return i;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                View layout = View.inflate(MainActivity.this,R.layout.layout,null);
                ImageView img = (ImageView)layout.findViewById(R.id.img);
                TextView textView1 = (TextView)layout.findViewById(R.id.name);
                TextView textView2 = (TextView)layout.findViewById(R.id.phone);
                TextView textView3 = (TextView)layout.findViewById(R.id.post);
                img.setImageResource(pic[i]);
                textView1.setText(values[i]);
                textView2.setText(values1[i]);
                textView3.setText(values2[i]);

                return layout;
            }
        };


        listView = (ListView)findViewById(R.id.list);
        listView.setAdapter(baseAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                t[i] += 1;
                String s = Integer.toString(t[i]);
                Toast.makeText(MainActivity.this, s ,Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }

        int id = item.getItemId();
        if (id == R.id.id1) {
            Toast.makeText(MainActivity.this,"設定",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.id2){
            Toast.makeText(MainActivity.this,"設定2",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.id3){
            Toast.makeText(MainActivity.this,"幫助1",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.id4){
            Toast.makeText(MainActivity.this,"幫助2",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}
