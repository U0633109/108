package com.example.android0730;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    String [] datas = {"一","二","三","四"};
    Spinner spinner;
    TextView textView;
//    String [] values={"陳振東","馬麗菁","張朝旭"};
//    int [] pic = {R.drawable.teacher03,R.drawable.teacher04,R.drawable.teacher05};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        BaseAdapter baseAdapter = new BaseAdapter() {
//            @Override
//            public int getCount() {
//                return values.length;
//            }
//
//            @Override
//            public Object getItem(int i) {
//                return i;
//            }
//
//            @Override
//            public long getItemId(int i) {
//                return i;
//            }
//
//            @Override
//            public View getView(int i, View view, ViewGroup viewGroup) {
//                View layout = View.inflate(MainActivity.this,R.layout.layout,null);
//                ImageView img = (ImageView)layout.findViewById(R.id.img);
//                TextView textView = (TextView)layout.findViewById(R.id.tv1);
//
//                img.setImageResource(pic[i]);
//                textView.setText(values[i]);
//
//                return layout;
//            }
//        };


        listView = (ListView)findViewById(R.id.list);
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,datas);
        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("標題");
                builder.setMessage(datas[i]);

                builder.setPositiveButton("確定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });

        spinner = (Spinner)findViewById(R.id.spinner);
        ArrayAdapter<String> arrayAdapter1 =new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item,datas);
        spinner.setAdapter(arrayAdapter1);

        textView = (TextView) findViewById(R.id.t1);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                textView.setText(datas[i]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setLogo(R.drawable.facebook);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
        }

        int id = item.getItemId();
        if (id == R.id.id1) {
            Toast.makeText(MainActivity.this,"設定",Toast.LENGTH_SHORT).show();
        }
        else if (id == R.id.id2){
            Toast.makeText(MainActivity.this,"幫助",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}
