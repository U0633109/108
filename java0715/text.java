import java.util.Scanner;
import java.util.Random;

public class text {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//1.****的迴圈練習
		Scanner sc = new Scanner(System.in);
		System.out.println("1.輸入個數");
        int n = sc.nextInt();
        if(n < 0){
            System.out.println("輸入有誤");
        }else{
            for(int i = 0; i < n; i ++){

                for(int z = n-1; z > i; z--){
                    System.out.print(" ");
                }
                for(int j = 0; j < i+1; j++) {
                    System.out.print("*");
                }
                System.out.println();
            }
        }
        
        //2.終極密碼
        System.out.println("2.猜數字1-100");
        Random r = new Random();
        int ans = r.nextInt(100)+1;
        int g = sc.nextInt();
        int min = 0;
        int max = 100;
        do{
            if(g > ans){
                System.out.println("猜太大");
                max = g;
                System.out.println(min + "~" + max);
            }
            if(g < ans){
                System.out.println("猜太小");
                min = g;
                System.out.println(min + "~" + max);
            }
            g = sc.nextInt();
        }while(g != ans);
        System.out.println("猜中了");
        
        //3.宣告一個任意長度的陣列，並用迴圈將陣列 內容反轉(1,2,3變成3,2,1)
        System.out.println("3.輸入長度");
        int n1 = sc.nextInt();
        int [] arr = new int[n1];
        for(int i = 0; i < arr.length; i++){
            arr[i] = r.nextInt(100)+1;
            System.out.print(arr[i]);
            if(i < arr.length-1){
                System.out.print(",");
            }
        }
        System.out.println();
        for(int i = arr.length-1; i >= 0; i--){
            System.out.print(arr[i]);
            if(i > 0){
                System.out.print(",");
            }
        }
        
	}
	
}
