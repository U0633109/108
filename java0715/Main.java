package com.company;

import java.util.Random;

public class Main {

    public static void create(String [][] arr){
        Random r = new Random();
        for(int i = 0; i < 5; i++){
            int Flush = r.nextInt(4)+1;
            switch (Flush){
                case 1:
                    arr[i][0] = "S";
                    break;
                case 2:
                    arr[i][0] = "H";
                    break;
                case 3:
                    arr[i][0] = "D";
                    break;
                case 4:
                    arr[i][0] = "C";
                    break;
            }

            int num = r.nextInt(13)+1;
            arr[i][1] = Integer.toString(num);
        }
    }

    public static String cardtype(String [][] arr){

        int f = 0; //花色
        int s = 0; //連續
        int k = 0; //數字一樣

        String type = "";

        for(int i = 0; i < 5; i++){
            if(arr[0][0].equals(arr[i][0])){
                f++;
            }
        }

        for(int i = 0; i < 5; i++){
            if( Integer.parseInt(arr[0][1]) == Integer.parseInt(arr[i][1]+i) ){
                s++;
            }
        }

        if(f == 5 && s == 5){
            type = "同花順";
        }else{
            if(s == 5){
                type = "順子";
            }
        }


        int m = 0;
        int n = 0;
        for(int j = 0; j < 5; j++){
            for(int i = 0; i < 5; i++) {
                if (arr[j][1].equals(arr[i][1])) {
                    k++;
                }
            }
            if(k > m){
                n = m;
                m = k;
            }
            k = 0;
        }

        switch (m) {
            case 4:
                type = "鐵支";
                break;
            case 3:
                if(n == 2){
                    type = "葫蘆";
                }else{
                    type = "三條";
                }
                break;
            case 2:
                if(m == n){
                    type = "兩對";
                }else {
                    type = "一對";
                }
                break;
            case 1:
                type = "散排";
                break;
        }

        return type;
    }

    public static void main(String[] args) {
	// write your code here

        String[][] A = new String[5][2];
        String[][] B = new String[5][2];

        create(A);
        create(B);

        for (int i = 0; i < 5; i++){
            System.out.print(A[i][0]+A[i][1]);
            if(i < 4){
                System.out.print(",");
            }
        }
        System.out.println();
        for (int i = 0; i < 5; i++){
            System.out.print(B[i][0]+B[i][1]);
            if(i < 4){
                System.out.print(",");
            }
        }

        System.out.println();

        String t1;
        String t2;
        t1 = cardtype(A);
        t2 = cardtype(B);

        System.out.print("A:"+t1+","+"B:"+t2);

        int p = 0,q = 0;
        switch (t1){
            case "同花順":
                p = 7;
                break;
            case "順子":
                p = 6;
                break;
            case "鐵支":
                p = 5;
                break;
            case "葫蘆":
                p = 4;
                break;
            case "三條":
                p = 3;
                break;
            case "兩對":
                p = 2;
                break;
            case "一對":
                p = 1;
                break;
            case "散排":
                p = 0;
                break;
        }

        switch (t2){
            case "同花順":
                q = 7;
                break;
            case "順子":
                q = 6;
                break;
            case "鐵支":
                q = 5;
                break;
            case "葫蘆":
                q = 4;
                break;
            case "三條":
                q = 3;
                break;
            case "兩對":
                q = 2;
                break;
            case "一對":
                q = 1;
                break;
            case "散排":
                q = 0;
                break;
        }

        System.out.println();

        if(p - q > 0){
            System.out.print("A大");
        }
        if(p - q < 0){
            System.out.print("B大");
        }
        if(p - q == 0){
            System.out.print("一樣大");
        }

     }
}
