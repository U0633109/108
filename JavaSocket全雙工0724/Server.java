import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    static ArrayList<String> name = new ArrayList<>();
    static ArrayList<MyThread> myth = new ArrayList<>();
    static ServerSocket ss;
    static Socket socket;
    static DataInputStream inputStream;

    public static class MyThread extends Thread{
        String n;
        Socket socket;
        DataOutputStream outputStream;
        DataInputStream inputStream;

        public MyThread(String n, Socket socket) throws IOException {
            this.n = n;
            this.socket = socket;
            outputStream = new DataOutputStream(socket.getOutputStream());
            inputStream = new DataInputStream(socket.getInputStream());
        }

        @Override
        public void run(){
            try {

                while (true) {
                    String msg = inputStream.readUTF();

                    for (int i = 0; i < name.size(); i++) {

                        if (name.get(i).equals(n)) {
                            if(msg.equals("exit")) {
                                name.remove(name.get(i));
                                myth.remove(myth.get(i));
                                i--;
                            }
                            continue;
                        } else {
                            if(msg.equals("exit")) {
                                myth.get(i).outputStream.writeUTF(n + "離開聊天室");
                            }else {
                                myth.get(i).outputStream.writeUTF(n + "：" + msg);
                            }
                        }
                    }
                }

            }catch (IOException e){
                System.out.println(n + "離開聊天室");
            }
        }
    }

    public static void main(String[] args) {
        try {
            ss = new ServerSocket(8000);
            System.out.println("開始傾聽...");
            while (true) {
                socket = ss.accept();

                inputStream = new DataInputStream(socket.getInputStream());
                String nm = inputStream.readUTF();
                System.out.println(nm + "加入聊天室。");
                name.add(nm);

                MyThread myThread = new MyThread(nm, socket);
                myth.add(myThread);
                myThread.start();

                for (int i = 0; i < name.size(); i++) {

                    if (name.get(i).equals(nm)) {
                        myth.get(i).outputStream.writeUTF("你加入了聊天室。");
                        continue;
                    } else {
                        myth.get(i).outputStream.writeUTF(nm + "加入聊天室");
                    }
                }
            }

        }catch (IOException e){
            System.out.println("結束通話");
        }
    }
}
