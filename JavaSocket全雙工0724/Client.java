import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    static Socket socket;
    static DataOutputStream outputStream;
    static DataInputStream inputStream;
    static Scanner sc = new Scanner(System.in);

    public static class MyThread extends Thread{
        @Override
        public void run(){
            try {
                while (true){
                    inputStream = new DataInputStream(socket.getInputStream());
                    String getmsg = inputStream.readUTF();//收訊息
                    System.out.println(getmsg);
                }
            }catch (IOException e){
                System.out.println("你離開聊天室");
            }
        }
    }

    public static void main(String[] args) {
        try {
            socket = new Socket("127.0.0.1",8000);
            MyThread thread = new MyThread();
            thread.start();
            System.out.print("請輸入暱稱：");
            outputStream = new DataOutputStream(socket.getOutputStream());
            String nm = sc.next(); //傳訊息
            outputStream.writeUTF(nm);
            while (true) {
                outputStream = new DataOutputStream(socket.getOutputStream());
                String sendmsg = sc.next(); //傳訊息
                if(sendmsg.equals("exit")){
                    outputStream.writeUTF(sendmsg);
                    socket.close();
                    break;
                }else {
                    outputStream.writeUTF(sendmsg);
                }
            }
            //System.exit(0);
        }catch (IOException e){
            System.out.println("你離開聊天室");
        }
    }
}
