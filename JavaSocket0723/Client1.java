import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Client1 {
    static Socket socket;
    static DataOutputStream outputStream;
    static DataInputStream inputStream;
    static Scanner sc = new Scanner(System.in);
    static boolean a = true;
    public static class MyThread extends Thread{
        @Override
        public void run(){
            try {
                for (;;) {
                    inputStream = new DataInputStream(socket.getInputStream());
                    String getmsg = inputStream.readUTF();//收訊息
                    System.out.println("Server：" + getmsg);
                    if (getmsg.equals("stop")) {
                        a = false;
                        break;
                    }
                }

                socket.close();
                System.exit(0);

            }catch (IOException e){
                System.out.println("結束通話1");
            }
        }
    }

    public static void main(String[] args) {
        try {
            socket = new Socket("127.0.0.1",8000);
            MyThread thread = new MyThread();
            thread.start();

            while (a) {
                outputStream = new DataOutputStream(socket.getOutputStream());
                String sendmsg = sc.next(); //傳訊息
                outputStream.writeUTF(sendmsg);
                System.out.println("Client：" + sendmsg);
                if(sendmsg.equals("stop"))break;
            }
            socket.close();
            System.exit(0);
        }catch (IOException e){
            System.out.println("結束通話");
        }
    }
}
