import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8000);
            System.out.println("開始傾聽...");
            Socket socket = serverSocket.accept(); //接受客戶端連線
            System.out.println("已有客戶端連線");

            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);

            for (;;){
                String getmsg = inputStream.readUTF(); //收訊息
                System.out.println("Client：" + getmsg);
                if(getmsg.equals("stop"))break;

                String sendmsg = sc.next(); //傳訊息
                outputStream.writeUTF(sendmsg);
                System.out.println("Server：" + sendmsg);
                if(sendmsg.equals("stop"))break;
            }

            socket.close();
            serverSocket.close();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
