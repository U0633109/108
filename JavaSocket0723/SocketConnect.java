import java.io.IOException;
import java.net.Socket;

public class SocketConnect {

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("www.google.com",80);

            System.out.println("本機位址：" + socket.getLocalAddress());
            System.out.println("本機port：" + socket.getLocalPort());
            System.out.println("遠端位址：" + socket.getInetAddress());
            System.out.println("遠端port：" + socket.getPort());
        }catch (IOException e){
            System.out.println("錯誤");
        }
    }

}
