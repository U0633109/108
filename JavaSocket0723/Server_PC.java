import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;

public class Server_PC {
    static ArrayList<Integer> total = new ArrayList<>();
    static Random r = new Random();

    public static boolean check(int g){
        boolean w = true;
        int [] x = new int[4];
        x[0] = g / 1000;
        x[1] = g / 100 % 10;
        x[2] = g / 10 % 10;
        x[3] = g % 10;

        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++){
                if (x[i] == x[j] && i != j)w =  false;
            }
        }
        return w;
    }


    public static void totalset(){
        for (int i = 1023; i <=9876; i++){
            if(check(i))total.add(i);
        }
    }

    public static String gAB(String a,ArrayList<Integer> pc){
        int g = Integer.parseInt(a);
        ArrayList<Integer> guess = new ArrayList<>();
        guess.add(g / 1000);
        guess.add(g / 100 % 10);
        guess.add(g / 10 % 10);
        guess.add(g % 10);

        int A = 0,B = 0;

        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++){
                if (guess.get(i) == pc.get(j)){
                    if (i == j)A++;
                    else B++;
                }
            }
        }

        return A + "A" + B + "B";
    }

    public static void setRndNumber(ArrayList<Integer> pc){
        int tempNumber = r.nextInt(10);
        pc.add(tempNumber) ;
        for(int i=1;i<4;i++){
            boolean lowFlag=true; //預設有重覆
            while(lowFlag){ //有重覆條件 , 重新取值
                lowFlag=false; //假設沒有重複
                tempNumber = r.nextInt(10);
                for(int j=0;j<i;j++){ //跟前幾位數字比對
                    if(pc.get(j) == tempNumber){ //如果有重覆 , 將lowFlag設為 true , 離開迴圈重新取值
                        lowFlag=true;
                        break;
                    }
                }
            }
            pc.add(tempNumber);
        }
    }


    public static void main(String[] args) {
        try {

            ServerSocket serverSocket = new ServerSocket(8000);
            System.out.println("等待玩家加入...");
            Socket socket = serverSocket.accept(); //接受客戶端連線
            System.out.println("已有玩家連線");


            ArrayList<Integer> pc = new ArrayList<>();
            setRndNumber(pc);
            System.out.println(pc);
            totalset();

            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());

            for (;;){

                String  sendmsg2 = String.valueOf(total.get(r.nextInt(total.size())));//傳訊息
                outputStream.writeUTF(sendmsg2);
                System.out.println("電腦猜：" + sendmsg2);

                String  A = inputStream.readUTF(); //收訊息
                String  B = inputStream.readUTF(); //收訊息
                System.out.println("Client：" + A + "A" + B + "B");

                for (int i = 0; i < total.size(); i++){
                    ArrayList<Integer> pk = new ArrayList<>();
                    pk.add(total.get(i) / 1000);
                    pk.add(total.get(i) / 100 % 10);
                    pk.add(total.get(i) / 10 % 10);
                    pk.add(total.get(i) % 10);
                    if(!(gAB(sendmsg2,pk).equals(A + "A" + B + "B"))){
                        total.remove(i);
                        i--;
                    }
                }
                System.out.println(total);
                if(A.equals("4"))break;


            }

            socket.close();
            serverSocket.close();

        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
