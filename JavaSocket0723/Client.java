import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1",8000);
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            Scanner sc = new Scanner(System.in);

            for (;;){
                String sendmsg = sc.next(); //傳訊息
                outputStream.writeUTF(sendmsg);
                System.out.println("Client：" + sendmsg);
                if(sendmsg.equals("stop"))break;

                String getmsg = inputStream.readUTF(); //收訊息
                System.out.println("Server：" + getmsg);
                if(getmsg.equals("stop"))break;
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
