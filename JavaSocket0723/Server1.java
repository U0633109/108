import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server1 {
    static ServerSocket serverSocket;
    static Socket socket;
    static DataOutputStream outputStream;
    static DataInputStream inputStream;

    public static class MyThread extends Thread{
        @Override
        public void run(){
            try {
                for (;;) {
                    inputStream = new DataInputStream(socket.getInputStream());
                    String getmsg = inputStream.readUTF();//收訊息
                    System.out.println("Client：" + getmsg);
                    if(getmsg.equals("stop"))break;
                }
                socket.close();
                serverSocket.close();
                System.exit(0);
            }catch (IOException e){
                System.out.println("結束通話1");
            }
        }
    }

    public static void main(String[] args) {
        try {
            serverSocket = new ServerSocket(8000);
            System.out.println("開始傾聽...");
            socket = serverSocket.accept(); //接受客戶端連線
            System.out.println("已有客戶端連線");
            MyThread thread = new MyThread();
            thread.start();
            for (;;) {
                outputStream = new DataOutputStream(socket.getOutputStream());
                Scanner sc = new Scanner(System.in);
                String sendmsg = sc.next(); //傳訊息
                outputStream.writeUTF(sendmsg);
                System.out.println("Server：" + sendmsg);
                if(sendmsg.equals("stop"))break;
            }
            socket.close();
            serverSocket.close();
            System.exit(0);

        }catch (IOException e){
            System.out.println("結束通話");
        }
    }
}
