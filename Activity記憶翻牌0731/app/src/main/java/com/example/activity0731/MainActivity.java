package com.example.activity0731;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    int size = 2;
    Button arrayButton[][];
    LinearLayout layoutOne,layoutTwo;
    int colorArray[]={Color.parseColor("#e6ffe6"), Color.parseColor("#ffffcc")};
    int hintNum[]={0,0,1,1};
    Button btOpen;
    int x = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutOne = findViewById(R.id.rOne);
        layoutTwo = findViewById(R.id.rTwo);
        createButton();
        shuffle();
    }

    void shuffle(){ //洗牌
        for (int i = 0; i < hintNum.length; i++){
            int current = hintNum[i];
            int ranNum = (int)(Math.random()*hintNum.length);
            hintNum[i] = hintNum[ranNum];
            hintNum[ranNum] = current;
        }
    }

    void createButton(){
        arrayButton = new Button[size][size];
        int count = 0;
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                arrayButton[i][j] = new Button(this);
                arrayButton[i][j].setHintTextColor(Color.parseColor("#00000000"));
                if(i == 0){
                    layoutOne.addView(arrayButton[i][j]);
                }else {
                    layoutTwo.addView(arrayButton[i][j]);
                }
                count++;
                final int finalI = i;
                final int finalJ = j;
                arrayButton[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        arrayButton[finalI][finalJ].setHint(String.valueOf(hintNum[finalI * 2 + finalJ]));

                        if (btOpen == null){
                            arrayButton[finalI][finalJ].setBackgroundColor(colorArray[hintNum[finalI * 2 + finalJ]]);
                            btOpen = arrayButton[finalI][finalJ];
                        }else{
                            arrayButton[finalI][finalJ].setBackgroundColor(colorArray[hintNum[finalI * 2 + finalJ]]);
                            if(arrayButton[finalI][finalJ].getHint() != btOpen.getHint()){

                                Handler cardHandler = new Handler();
                                cardHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        arrayButton[finalI][finalJ].setBackgroundColor(Color.parseColor("#d9d9d9"));
                                        btOpen.setBackgroundColor(Color.parseColor("#d9d9d9"));
                                        btOpen = null;
                                    }
                                },1000);
                            }else {
                                btOpen.setEnabled(false);
                                arrayButton[finalI][finalJ].setEnabled(false);
                                btOpen = null;
                            }


                        }
                    }
                });

            }
        }
    }


}
