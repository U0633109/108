public class HW2 {

    public static abstract class Animal{
        String name;

        public Animal() {
            name = "animal";
        }

        public void setName(String n) {
            name = n;
        }

        public abstract void move();
        public abstract void sound();

    }

    public static void main(String[] args) {

        Animal a =
                new Animal() {
                    @Override
                    public void move() {
                        System.out.println("Move: walk");
                    }

                    @Override
                    public void sound() {
                        System.out.println("Sound: rrrrr");
                    }
                };

        a.move();
        a.sound();
    }

}
