import java.util.ArrayList;
import java.util.Collections;


public class HW3 {

    static boolean mode = true;

    public static class Account implements Comparable<Account>{
        String name;
        int balance;


        public Account(String name, int balance) {
            this.name = name;
            this.balance = balance;
        }

        public String getName() {
            return name;
        }

        public int getBalance() {
            return balance;
        }

        @Override
        public String toString() {
            return "{" +
                    "name='" + name + '\'' +
                    ", balance=" + balance +
                    '}';
        }

        @Override
        public int compareTo(Account a) {
            if(mode){
                return this.getBalance() - a.getBalance();
            }else{
                return this.getName().compareTo(a.getName());
            }
        }

    }

    public static void main(String[] args) {
        Account a = new Account("Mary",30);
        Account b = new Account("Gary",78);
        Account c = new Account("Tery",66);
        Account d = new Account("Ben",1200);
        Account e = new Account("Alice",18);

        ArrayList<Account> arr = new ArrayList<Account>();
        arr.add(a);
        arr.add(b);
        arr.add(c);
        arr.add(d);
        arr.add(e);
        mode = true;
        Collections.sort(arr);

        for (Account i : arr){
            System.out.println(i.toString());
        }

        System.out.println();
        mode = false;
        Collections.sort(arr);

        for (Account i : arr){
            System.out.println(i.toString());
        }

    }

}
