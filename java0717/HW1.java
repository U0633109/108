import java.util.Scanner;

public class HW1 {

    public static class Account{
        int balance;

        public Account() {

        }

        public void credit(int money){
            balance = balance + money;
        }

        public void debit(int money){
            balance = balance - money;
        }
    }

    public static class SavingAccount extends Account{
        double interestRate;

        public SavingAccount() {

        }

        public void setInterestRate(double interestRate) {
            this.interestRate = interestRate;
        }

        public double getInterestRate() {
            return interestRate;
        }

        public void calculateInterest(int time){
            double Interest;
            Interest = balance * interestRate * time;
            balance = balance + (int)(Interest);
        }
    }

    public static class CheckAccount extends Account{
        double transactionFee;

        public CheckAccount(double transactionFee) {
            this.transactionFee = transactionFee;
        }

        public void credit(int money){
            balance = balance + money - chargeFee(money);
        }

        public void debit(int money){
            balance = balance - money - chargeFee(money);
        }

        public int chargeFee(int money){
            int m = (int)(money * transactionFee);
            return m;
        }

    }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int m = 0;

        System.out.println("儲蓄帳戶");
        SavingAccount a = new SavingAccount();

        do{
            System.out.println("1.存款 2.提款 3.設定利率 4.讀取利率 5.計算利率 6.查看帳戶餘額 7.exit");
            m = sc.nextInt();
            switch (m){
                case 1:
                    a.credit(sc.nextInt());
                    break;
                case 2:
                    a.debit(sc.nextInt());
                    break;
                case 3:
                    a.setInterestRate(sc.nextDouble());
                    break;
                case 4:
                    System.out.println(a.getInterestRate());
                    break;
                case 5:
                    System.out.println("利率時間?");
                    a.calculateInterest(sc.nextInt());
                    break;
                case 6:
                    System.out.println(a.balance);
                    break;
            }
        }while (m != 7);

        System.out.println("支票帳號的手續費?");
        CheckAccount b = new CheckAccount(sc.nextDouble());

        m = 0;
        do{
            System.out.println("1.存款 2.提款 3.查看帳戶餘額 4.exit");
            m = sc.nextInt();
            switch (m){
                case 1:
                    b.credit(sc.nextInt());
                    break;
                case 2:
                    b.debit(sc.nextInt());
                    break;
                case 3:
                    System.out.println(b.balance);
                    break;
            }
        }while (m != 4);

    }

}
