<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>出貨單</title>
</head>
<body>
<table border="1">
<tr>
<td>出貨編號</td>
<td>書本編號</td>
<td>單價</td>
<td>數量</td>
<td>小計</td>
<td>客戶名稱</td>
<td>出貨日期</td>
</tr>
<?php
	require_once("dbtools.inc.php");
	$link = create_connection();//建立連線
	$sql0 = "SELECT* FROM 出貨紀錄表 Order by 出貨編號 ASC"; 
	$result0 = execute_sql($link, "book", $sql0);	
	
	if (isset($_POST["Shipment"])){
		$num = mysqli_num_rows($result0); 
		$sql = "INSERT INTO 出貨紀錄表(出貨編號,書本編號,單價,數量,小計,客戶名稱,出貨日期) VALUES ('";
		$sql.= ++$num."','".$_POST["id"]."','".$_POST["price"]."','".$_POST["qu"]."','".$_POST["price"]*$_POST["qu"]
		."','".$_POST["gname"]."','".$_POST["date"]."')";
		$result = execute_sql($link, "book", $sql);
		
		$sql2 = "SELECT* FROM 庫存表 Where 書本編號=".$_POST["id"];
		$result = execute_sql($link, "book", $sql2);	
		$row = mysqli_fetch_array($result);
		$q = $row["庫存數量"]-$_POST["qu"];
		
		$sql2="UPDATE 庫存表 SET 庫存數量=" .$q. " WHERE 書本編號=".$_POST["id"];
		
		$result = execute_sql($link, "book", $sql2);
	}

	while ($row0 = mysqli_fetch_row($result0)){
		echo "<tr>";	
		
		for($i = 0; $i < mysqli_num_fields($result0); $i++){
			echo "<td>$row0[$i]</td>";	
		}
		
		echo "</tr>";
	}
	
?>

<form action="Shipment.php" method="post">
出貨紀錄單<br><br>
書本編號：
<select name="id">
<?php
	require_once("dbtools.inc.php");
	$link = create_connection();//建立連線
	$sql2 = "SELECT* FROM 庫存表";
	$result = execute_sql($link, "book", $sql2);	
	while($row = mysqli_fetch_row($result)){
		echo "<option value=".$row[0].">".$row[0]."</option>";
	}
?>	
</select><br><br>
單價：<input name="price" type="text" maxlength="3"/><br><br>
數量：<input name="qu" type="text" maxlength="3"/><br><br>
客戶名稱：<input name="gname" type="text" maxlength="3"/><br><br>
出貨日期：<input name="date" type="date"/><br><br>

<input name="Shipment" type="submit" value="送出"/><br><br>
</form>
</table>
<a href = "inventory.php">目前庫存表</a>
</body>
</html>