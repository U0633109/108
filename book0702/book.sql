-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- 主機: 127.0.0.1
-- 產生時間： 
-- 伺服器版本: 10.1.10-MariaDB
-- PHP 版本： 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `book`
--

-- --------------------------------------------------------

--
-- 資料表結構 `出貨紀錄表`
--

CREATE TABLE `出貨紀錄表` (
  `出貨編號` int(3) NOT NULL,
  `書本編號` int(3) NOT NULL,
  `單價` int(3) NOT NULL,
  `數量` int(3) NOT NULL,
  `小計` int(5) NOT NULL,
  `客戶名稱` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `出貨日期` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `出貨紀錄表`
--

INSERT INTO `出貨紀錄表` (`出貨編號`, `書本編號`, `單價`, `數量`, `小計`, `客戶名稱`, `出貨日期`) VALUES
(1, 101, 20, 60, 1200, '王壬', '2019-07-02'),
(2, 105, 25, 30, 750, '槙滇', '2019-07-17'),
(3, 123, 23, 30, 690, '23', '0000-00-00'),
(4, 213, 21, 5, 105, '213', '2019-07-08'),
(5, 123, 213, 5, 1065, '213', '2019-07-10'),
(6, 123, 213, 5, 1065, '213', '2019-07-02');

-- --------------------------------------------------------

--
-- 資料表結構 `庫存表`
--

CREATE TABLE `庫存表` (
  `書本編號` int(3) NOT NULL,
  `書名` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `庫存數量` int(3) NOT NULL,
  `進貨日期` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `庫存表`
--

INSERT INTO `庫存表` (`書本編號`, `書名`, `庫存數量`, `進貨日期`) VALUES
(101, '拉拉日記', 40, '2019-07-18'),
(105, '蝸牛成長記', 40, '2019-07-25'),
(213, '2123', 208, '2019-07-02'),
(222, '213', 1, '2019-07-10'),
(453, '264', 10, '2019-07-10');

-- --------------------------------------------------------

--
-- 資料表結構 `進貨紀錄表`
--

CREATE TABLE `進貨紀錄表` (
  `進貨編號` int(3) NOT NULL,
  `書本編號` int(3) NOT NULL,
  `類別` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `書名` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `單價` int(3) NOT NULL,
  `數量` int(3) NOT NULL,
  `進貨日期` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- 資料表的匯出資料 `進貨紀錄表`
--

INSERT INTO `進貨紀錄表` (`進貨編號`, `書本編號`, `類別`, `書名`, `單價`, `數量`, `進貨日期`) VALUES
(1, 101, '文學', '拉拉日記', 10, 50, '2019-07-17'),
(2, 105, '文學', '蝸牛成長記', 20, 70, '2019-07-25'),
(3, 101, '文學', '拉拉日記', 10, 50, '2019-07-18'),
(4, 123, '冒險', '789', 999, 1, '2019-07-28'),
(5, 123, '435', '1231', 321, 32, '2019-07-09'),
(6, 222, '123', '213', 321, 1, '2019-07-10'),
(7, 213, '213', '2123', 321, 213, '2019-07-02'),
(8, 123, '312', '213', 321, 7, '2019-07-03'),
(9, 453, '263', '264', 456, 10, '2019-07-10');

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `出貨紀錄表`
--
ALTER TABLE `出貨紀錄表`
  ADD PRIMARY KEY (`出貨編號`);

--
-- 資料表索引 `庫存表`
--
ALTER TABLE `庫存表`
  ADD PRIMARY KEY (`書本編號`);

--
-- 資料表索引 `進貨紀錄表`
--
ALTER TABLE `進貨紀錄表`
  ADD PRIMARY KEY (`進貨編號`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
