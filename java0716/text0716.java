package com.company;

import java.util.ArrayList;
import java.util.Scanner;

class BigNumber{
    String str;
    ArrayList<String> arr = new ArrayList<String>();

    public BigNumber(String str) {

        String a [] = str.split("");


        for(int i=0;i<a.length;i++){
            arr.add(a[i]);
        }

    }

    public BigNumber add(BigNumber c) {
        int l = arr.size() - c.arr.size();

        if(l > 0){
            for(int i = 0; i < l; i++) {
                c.arr.add(0, "0");
            }
        }else {
            for (int i = 0; i < Math.abs(l); i++) {
                arr.add(0, "0");
            }
        }

        BigNumber sum = new BigNumber("");
        sum.arr.remove("");

        for(int i = 0; i < arr.size(); i++){
            int s = Integer.parseInt(arr.get(i)) + Integer.parseInt(c.arr.get(i));
            sum.arr.add(Integer.toString(s));
        }

        for(int i = sum.arr.size()-1; i >0 ; i--){
            if(Integer.parseInt( sum.arr.get(i) ) > 9){
                int x = Integer.parseInt(sum.arr.get(i - 1)) + 1;
                sum.arr.set(i - 1, Integer.toString(x));
                x = Integer.parseInt(sum.arr.get(i)) - 10;
                sum.arr.set(i, Integer.toString(x));
            }
        }

        return sum;
    }

    public BigNumber whichbig(BigNumber a,BigNumber b){
        int l = a.arr.size() - b.arr.size();

        if(l > 0){
            for(int i = 0; i < l; i++) {
                b.arr.add(0, "0");
            }
            return a.less(b);
        }else {
            if(l == 0){

                for(int i = 0; i < a.arr.size(); i++){
                    if(Integer.parseInt(a.arr.get(i)) >  Integer.parseInt(b.arr.get(i))){
                        return a.less(b);
                    }else{
                        return b.less(a);
                    }
                }

            }else{
                for (int i = 0; i < Math.abs(l); i++) {
                    a.arr.add(0, "0");
                }
                return b.less(a);
            }
        }
        return a.less(b);
    }

    public BigNumber less(BigNumber c) {
        BigNumber sub = new BigNumber("");
        sub.arr.remove("");

        for(int i = 0; i < arr.size(); i++){
            int s = Integer.parseInt(arr.get(i)) - Integer.parseInt(c.arr.get(i));
            sub.arr.add(Integer.toString(s));
        }

        for(int i = sub.arr.size()-1; i >0 ; i--){
            if(Integer.parseInt( sub.arr.get(i) ) < 0){
                int x = Integer.parseInt(sub.arr.get(i - 1)) - 1;
                sub.arr.set(i - 1, Integer.toString(x));
                x = Integer.parseInt(sub.arr.get(i)) + 10;
                sub.arr.set(i, Integer.toString(x));
            }
        }

        for(int i = 0; i < sub.arr.size(); i++){
            if(sub.arr.get(i).equals("0")){
                sub.arr.remove(i);
                i--;
            }else{
                break;
            }
        }

        return sub;
    }

    public void show(){
        for(String s: arr){
            System.out.print(s);
        }
    }

}


public class text0716 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        BigNumber a = new BigNumber(sc.next());
        BigNumber b = new BigNumber(sc.next());

        BigNumber sum = new BigNumber("");
        sum.arr.remove("");

        sum = a.add(b);
        sum.show();

        System.out.println();

        BigNumber sub = new BigNumber("");
        sub.arr.remove("");

        sub = sub.whichbig(a,b);
        sub.show();

    }

}
