package com.company;

import java.util.HashMap;
import java.util.Scanner;

class Country{
    HashMap<String, String> country = new HashMap<>();
    HashMap<String, Integer> time = new HashMap<>();

    public Country() {
        country.put("東京迪士尼","35°37N,139°52E");
        country.put("台北101","25°02N,121°33E");
        country.put("法國艾菲爾鐵塔","48°51N,2°17E");
        country.put("美國黃石國家公園","44°36N,110°30W");
        time.put("東京迪士尼",9);
        time.put("台北101",8);
        time.put("法國艾菲爾鐵塔",1);
        time.put("美國黃石國家公園",-6);
    }

    public int Sub(String a,String b){
        int sub = time.get(a) - time.get(b);
        return sub;
    }

}

public class HW4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Country a = new Country();
        System.out.println("A.時差 B.經緯度 C.exit");
        String m = sc.next();
        do{
            switch (m){
                case "A":
                    System.out.println("1.東京迪士尼 2.台北101 3.法國艾菲爾鐵塔 4.美國黃石國家公園");
                    System.out.println(a.Sub(sc.next(),sc.next()));
                    break;
                case "B":
                    System.out.println("1.東京迪士尼 2.台北101 3.法國艾菲爾鐵塔 4.美國黃石國家公園");
                    System.out.println(a.country.get(sc.next()));
                    break;
            }
            System.out.println("A.時差 B.經緯度 C.exit");
            m = sc.next();
        }while(!m.equals("C"));







    }
}
