package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

class stack{
    ArrayList<String> arr = new ArrayList<String>();

    public stack() {

    }

    public void push(String str){
        arr.add(str);
        System.out.println("放入"+str);
    }

    public void pop(){
        System.out.println("拿出"+arr.get(arr.size()-1));
        arr.remove(arr.size()-1);
    }

    public void show(){
        for(String s: arr){
            System.out.println(s);
        }
    }

}

class queue{
    LinkedList<String> arr = new LinkedList<String>();
    public queue() {

    }
    public void push(String str){
        arr.add(str);
        System.out.println("放入"+str);
    }

    public void pop(){
        System.out.println("拿出"+arr.getFirst());
        arr.removeFirst();
    }

    public void show(){
        for(String s: arr){
            System.out.println(s);
        }
    }
}

public class HW3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("堆疊");
        stack a = new stack();
        System.out.println("1.push 2.pop 3.show 4.exit");
        int m = sc.nextInt();

        while(m != 4) {
            switch (m) {
                case 1:
                    a.push(sc.next());
                    break;
                case 2:
                    a.pop();
                    break;
                case 3:
                    a.show();
                    break;
            }
            m = sc.nextInt();
        }

        System.out.println("伫列");
        queue b = new queue();
        System.out.println("1.push 2.pop 3.show 4.exit");
        int n = sc.nextInt();

        while(n != 4) {
            switch (n) {
                case 1:
                    b.push(sc.next());
                    break;
                case 2:
                    b.pop();
                    break;
                case 3:
                    b.show();
                    break;
            }
            n = sc.nextInt();
        }

    }
}
