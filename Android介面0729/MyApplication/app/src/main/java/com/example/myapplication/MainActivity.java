package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView t,ans;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t = findViewById(R.id.t1);
        ans = findViewById(R.id.ans);
    }

    protected void buttonOnClick(View view) {
        Button button = (Button) view;
        t.setText(t.getText().toString() + button.getText());
    }
    protected void deleteOnClick(View view) {
        t.setText("計算式：");
        ans.setText("答案：");
    }
    protected void equalOnClick(View view) {
        String s = t.getText().toString().substring(4);
        char symbol = 0;
        int n = 0;
        String s1 = "", s2 = "";
        for (int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '+' || s.charAt(i) == '-' || s.charAt(i) == '*' || s.charAt(i) == '/'){
                symbol = s.charAt(i);
                n = i;
                break;
            }
        }

        s1 = s.substring(0,n);
        s2 = s.substring(n+1);

        switch (symbol){
            case '+':
                n = Integer.parseInt(s1) + Integer.parseInt(s2);
                break;
            case '-':
                n = Integer.parseInt(s1) - Integer.parseInt(s2);
                break;
            case '*':
                n = Integer.parseInt(s1) * Integer.parseInt(s2);
                break;
            case '/':
                n = Integer.parseInt(s1) / Integer.parseInt(s2);
                break;
        }

        ans.setText("答案：" + n);

    }
}
