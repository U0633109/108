package com.company;

public class HW3_2 {

    public static class Count{
        public int n;
    }

    public static class MyThread extends Thread{
        static Count count = new Count();
        @Override
        public void run(){
            for (int i = 0; i < 600; i++){
                synchronized (count) {
                    count.n += 1;
                    System.out.println(this.getName() + ":" + count.n);
                }
            }
        }
    }

    public static void main(String[] args) {
        MyThread thread = new MyThread();
        MyThread thread1 = new MyThread();
        thread.start();
        thread1.start();
    }
}
