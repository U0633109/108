package com.company;

import java.util.Calendar;
import java.util.Scanner;

public class HW2 {

    public static class MyThread extends Thread{
        static int t = 1;
        @Override
        public void run(){
            Calendar c = Calendar.getInstance();


            for (int i = 0; i < Integer.MAX_VALUE; i++){
                c.setTimeInMillis(System.currentTimeMillis());
                int hour = c.get(Calendar.HOUR_OF_DAY);
                int minute = c.get(Calendar.MINUTE);
                int second = c.get(Calendar.SECOND);

                System.out.println(hour + ":" + minute + ":" + second);
                try{
                    Thread.sleep(1000*t);
                }catch (InterruptedException e){
                    System.out.println("end");
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        MyThread thread = new MyThread();
        thread.start();

        for(;;) {
            String x = sc.next();
            String y = "stop";
            if (x.equals(y)) {
                thread.interrupt();
                break;
            }
            thread.t = Integer.parseInt(x);
        }

    }

}
