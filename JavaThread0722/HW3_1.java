package com.company;

public class HW3_1 {

    public static class Test extends Thread{
        static int n = 0;
        @Override
        public void run(){
            for (int i = 0; i < 600; i++){
                Test.n += 1;
                System.out.println(this.getName()+ ":" + n);
            }
        }
    }

    public static void main(String[] args) {
        Test a = new Test();
        Test b = new Test();
        a.start();
        b.start();
    }

}
