package com.company;

import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

public class HW1 {

    public static void main(String[] args) {

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        int second = c.get(Calendar.SECOND);

        System.out.println(hour + ":" + minute + ":" + second);

        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        System.out.println("猜數字1-100");
        int ans = r.nextInt(100)+1;
        int g = sc.nextInt();
        int min = 0;
        int max = 100;
        do{
            if(g > ans){
                System.out.println("猜太大");
                max = g;
                System.out.println(min + "~" + max);
            }
            if(g < ans){
                System.out.println("猜太小");
                min = g;
                System.out.println(min + "~" + max);
            }
            g = sc.nextInt();
        }while(g != ans);
        System.out.println("猜中了");
        c.setTimeInMillis(System.currentTimeMillis());
        hour = c.get(Calendar.HOUR_OF_DAY)-hour;
        minute = c.get(Calendar.MINUTE)-minute;
        second = c.get(Calendar.SECOND)-second;
        if(second < 0){
            second += 60;
            minute --;
        }
        System.out.println("花費 " + hour + ":" + minute + ":" + second);
    }

}
